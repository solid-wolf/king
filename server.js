var express = require('express');

var app = express();

var fs = require('fs');

var bodyParser = require('body-parser');

app.use('/public', express.static('public'));



app.get('/formulairedecont/formulaire.html', function (req, res) {
  res.sendfile('./formulairedecont/formulaire.html');
  
});

app.use(express.json());


app.post('/formulairedecont/formulaire.html', function(req, res){
  let datas = req.body;
  let data = JSON.stringify(datas, null);
  fs.writeFileSync('donnees.json', data);
  //reponse.
});

app.get('/origines/origine.html', function (req, res) {
  res.sendfile('./origines/origine.html');
  
});

app.get('/huguescapet/hugues.html', function (req, res) {
  res.sendfile('./huguescapet/hugues.html');
});

app.get('/louisIX/louis.html', function (req, res) {
  res.sendfile('./louisIX/louis.html');
});

app.get('/philippeauguste/philippea.html', function (req, res) {
  res.sendfile('./philippeauguste/philippea.html');
});

app.get('/philippelebel/philippeb.html', function (req, res) {
  res.sendfile('./philippelebel/philippeb.html');
});

app.listen(8080);